import React from "react";
import { render, screen } from "@testing-library/react";
import axios from "axios";
import ExampleAxios from "./App";

jest.mock("axios");

const mockedResponse = {
  data: {
    name: "John Doe",
    email: "johndoe@example.com",
  },
};

beforeEach(() => {
  axios.get.mockResolvedValue(mockedResponse);
});

test("renders user data fetched from API", async () => {
  render(<ExampleAxios />);

  expect(screen.getByText("Loading...")).toBeInTheDocument();

  const userData = await screen.findByText(/John Doe/i);
  expect(userData).toBeInTheDocument();
});
