import { exampleAsyncFunction } from './async-function'

it('ex1', async () => {
    const response = await exampleAsyncFunction()
    expect(response).toEqual('success')
})
