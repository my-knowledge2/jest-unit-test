let a;
let b;

beforeAll(() => {
    a = 10;
    b = 9;
})

describe('EX ALL 4.1', () => { 
    test('a should to be 10', () => {
        expect(a).toBe(10)
    });
});

describe('EX ALL 4.2', () => { 
    test('b should to be 9', () => {
        expect(b).toBe(9)
    });
});

  
afterAll(() => {
    a = 0;
    b = 9;
})