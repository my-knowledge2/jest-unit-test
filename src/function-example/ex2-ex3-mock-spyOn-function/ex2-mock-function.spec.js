// it('ex2', async () => {
//     const response = await timeoutFunction()
//     expect(response).toEqual('success')
// })
  
it('ex2 with jest mock', () => {
    myMock = jest.fn()
    myMock.mockResolvedValue("Yes I'am Mock");
  
    expect(myMock()).resolves.toEqual("Yes I'am Mock")
})