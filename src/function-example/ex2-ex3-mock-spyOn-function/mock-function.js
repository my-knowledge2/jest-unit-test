import { timeoutFunction } from './timeout-function'

export const mockTimeoutFunctionEX2 = async () => {
    
    const callTimeoutFunction = await timeoutFunction();
    return callTimeoutFunction === 'success';

}

export default { mockTimeoutFunctionEX2 }
  