
import { mockTimeoutFunctionEX2 } from './mock-function';
import * as timeoutFunctionModule from './timeout-function';

// it('ex3 mock without SpyOn', async () => {
//     let myMock = await mockTimeoutFunctionEX2()
//     expect(myMock).toBe(true);
// })

it('ex3 with spyOn case Failure', async () => {
    const spy = jest.spyOn(timeoutFunctionModule, 'timeoutFunction')
    spy.mockReturnValue("failure");
    const result = await mockTimeoutFunctionEX2()

    expect(result).toBe(false);
    expect(spy).toHaveBeenCalledTimes(1);
    spy.mockRestore();
})  

it('ex3 with spyOn case Success', async () => {
    const spy = jest.spyOn(timeoutFunctionModule, 'timeoutFunction')
    spy.mockReturnValue("success");
    const result = await mockTimeoutFunctionEX2()

    expect(result).toBe(true);
    expect(spy).toHaveBeenCalledTimes(1);
    spy.mockRestore();
})  